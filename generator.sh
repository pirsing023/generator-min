#!/bin/bash

pasta_raiz="app/views"
extensao_js=".js"
extensao_css=".css"
closure_compiler_version="v20231112"

echo "Monitorando altera��es em arquivos $extensao_js e $extensao_css em $pasta_raiz e subpastas"

while true; do
    arquivo_modificado_js=$(find $pasta_raiz -type f -name "*$extensao_js" -mmin -0,0167)
    arquivo_modificado_css=$(find $pasta_raiz -type f -name "*$extensao_css" -mmin -0,0167)

    if [ -n "$arquivo_modificado_js" ]; then        
        diretorio=$(dirname "$arquivo_modificado_js")
        nome_arquivo_sem_extensao=$(basename "$arquivo_modificado_js" .js)
        caminho_arquivo_minificado="$diretorio/$nome_arquivo_sem_extensao.min.js"

        if [[ "$nome_arquivo_sem_extensao" == *".min"* ]]; then
            continue
        fi

        java -jar closure-compiler-$closure_compiler_version.jar --js "$arquivo_modificado_js" --compilation_level SIMPLE_OPTIMIZATIONS --charset ISO-8859-1 --js_output_file "$caminho_arquivo_minificado"

        status_original=$(git status --short $arquivo_modificado_js)

        status_min=$(git status --short "${arquivo_modificado_js%.js}.min.js")

        if [ -n "$status_min" ] && [ -z "$status_original" ]; then
            git checkout -- "${arquivo_modificado_js%.js}.min.js"
        fi
    fi

    if [ -n "$arquivo_modificado_css" ]; then
        diretorio=$(dirname "$arquivo_modificado_css")
        nome_arquivo_sem_extensao=$(basename "$arquivo_modificado_css" .css)
        caminho_arquivo_minificado="$diretorio/$nome_arquivo_sem_extensao.min.css"

        if [[ "$nome_arquivo_sem_extensao" == *".min"* ]]; then
            continue
        fi

        java -jar yuicompressor-2.4.8.jar "$arquivo_modificado_css" -o "$caminho_arquivo_minificado" --charset ISO-8859-1

        status_original=$(git status --short $arquivo_modificado_css)

        status_min=$(git status --short "${arquivo_modificado_css%.css}.min.css")

        if [ -n "$status_min" ] && [ -z "$status_original" ]; then
            git checkout -- "${arquivo_modificado_css%.css}.min.css"
        fi

    fi

    sleep 1
done
